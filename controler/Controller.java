package controler;

import java.util.ArrayList;
import model.ConsultaMedica;

public class Controller {

		ArrayList<ConsultaMedica> listaConsultas;
		
		public Controller() {
			listaConsultas = new ArrayList<ConsultaMedica>();
		}
	
		public void marcarConsulta(ConsultaMedica umaConsulta) {
			listaConsultas.add(umaConsulta);
		}
		
		public void retirarConsulta(ConsultaMedica umaConsulta) {
			listaConsultas.remove(umaConsulta);
		}
		
		public ConsultaMedica buscaConsulta(String nomePaciente) {
			for(ConsultaMedica umaConsulta: listaConsultas) {
				if(umaConsulta.getNomePaciente().equalsIgnoreCase(nomePaciente)) {
					return umaConsulta;
				}
			}
			return null;
		}
}
