package model;

public class ConsultaMedica {

		private String horario;
		private String diagnostico;
		private Medico medico;
		private Paciente paciente;
		private String nomePaciente;
		
		public Medico getMedico() {
			return medico;
		}
		public void setMedico(Medico medico) {
			this.medico = medico;
		}
		public Paciente getPaciente() {
			return paciente;
		}
		public void setPaciente(Paciente paciente) {
			this.paciente = paciente;
		}
		public String getNomePaciente() {
			return nomePaciente;
		}
		public void setNomePaciente() {
			nomePaciente = paciente.getNome();
		}
		public String getHorario() {
			return horario;
		}
		public void setHorario(String horario) {
			this.horario = horario;
		}
		public String getDiagnostico() {
			return diagnostico;
		}
		public void setDiagnostico(String diagnostico) {
			this.diagnostico = diagnostico;
		}
		
}
