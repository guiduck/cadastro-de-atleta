package model;

public class Medico extends Usuario implements Autenticavel{

	public Medico(String nome, String doc) {
		if (DocumentoValido) {
			super(nome, doc);
		}
	}
	
	public boolean Autenticado (String senha) {
	}
		

	private String especializacao, convenio;

	public String getEspecializacao() {
		return especializacao;
	}

	public void setEspecializacao(String especializacao) {
		this.especializacao = especializacao;
	}

	public String getConvenio() {
		return convenio;
	}

	public void setConvenio(String convenio) {
		this.convenio = convenio;
	}
	
	

}
