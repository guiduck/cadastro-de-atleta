package model;

public abstract class Usuario {

	protected String documento;
	public String nome;
	private String area;
	protected String senha;
	
	public Usuario(String nome, String doc) {
	}
	
	public boolean DocumentoValido (String doc) {
		if (doc != null)
			return true;
		else
			return false;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}	
}
